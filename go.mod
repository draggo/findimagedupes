module gitlab.com/draggo/findimagedupes

require (
	github.com/facebookgo/symwalk v0.0.0-20150726040526-42004b9f3222
	github.com/mattn/go-sqlite3 v1.13.0
	github.com/rakyll/magicmime v0.1.0
	gitlab.com/opennota/phash v0.0.0-20180912085150-c347865b02bf
)

go 1.13
